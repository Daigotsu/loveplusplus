var config = require('../config/config');
var MongoClient = require('mongodb').MongoClient;
var async = require('async');
MongoClient.connect(config.mongo.dburi, {db:{}, server:{auto_reconnect:true, poolSize:1}}, function (err, db) {
    if (err) {
        console.log('error connecting to mongo');
        throw err;
    }
    //Do async indexes
    //Do parallel async
    console.log('begin indexing');
    async.parallel(
    [
        function (callback) {
            //Set registration time index
            db.collection('profiles', function (err, collection) {
                if (!err) {
                    console.log('setting profile email');
                    collection.ensureIndex({ "email":1 }, callback);
                }
            });
        },
        function (callback) {
            //Set registration time index
            db.collection('profiles', function (err, collection) {
                if (!err) {
                    console.log('setting profile type');
                    collection.ensureIndex({ "type":1 }, callback);
                }
            });
        },
        function (callback) {
            //Set registration time index
            db.collection('profiles', function (err, collection) {
                if (!err) {
                    console.log('setting profile type');
                    collection.ensureIndex({ "_validated":1 }, callback);
                }
            });
        },
        function (callback) {
            //Set registration time index
            db.collection('registrations-times', function (err, collection) {
                if (!err) {
                    console.log('setting registration time index');
                    collection.ensureIndex({ "date":1 }, { expireAfterSeconds:1800 }, callback);
                }
            });
        },
        function (callback) {
            //Ensure collection index
            db.collection('csrf-tokens', function (err, collection) {
                if (!err) {
                    console.log('setting csrf index');
                    collection.ensureIndex({ "issued":1}, { expireAfterSeconds:1800/*30 minutes*/ }, callback);
                }
            });
        },
        function (callback) {
            db.collection('users-online', function (err, collection) {
                if (!err) {
                    console.log('dropping old user-online index');
                    collection.dropIndex("type", callback);
                }
            });
        },
        function (callback) {
            db.collection('users-online', function (err, collection) {
                if (!err) {
                    console.log('setting user-online index');
                    collection.ensureIndex({"lastseen":1}, { expireAfterSeconds:120/*2 minutes*/ }, callback);
                }
            });
        },
        function (callback) {
            db.collection('chat-tokens', function (err, collection) {
                if (!err) {
                    console.log('setting chat index');
                    collection.ensureIndex({ "issued":1}, { expireAfterSeconds:3600 * 6/*6 hours*/ }, callback);
                }
            });
        },
        function (callback) {
            db.collection('messages', function (err, collection) {
                if (!err) {
                    console.log('setting messages index');
                    collection.ensureIndex({ from:1, to:1, read:1}, { }, callback);
                }
            });
        },
        function (callback) {
            db.collection('messages', function (err, collection) {
                if (!err) {
                    console.log('setting messages index');
                    collection.ensureIndex({ from:1}, { }, callback);
                }
            });
        },
        function (callback) {
            db.collection('messages', function (err, collection) {
                if (!err) {
                    console.log('setting messages index');
                    collection.ensureIndex({last_notify:1}, { }, callback);
                }
            });
        },
        function (callback) {
            db.collection('messages', function (err, collection) {
                if (!err) {
                    console.log('setting messages index');
                    collection.ensureIndex({ to:1}, { }, callback);
                }
            });
        },
        function (callback) {
            db.collection('messages', function (err, collection) {
                if (!err) {
                    console.log('setting messages ttl index');
                    collection.ensureIndex({ received:1}, { expireAfterSeconds:3600 * 24*7/*1 week*/ }, callback);
                }
            });
        }
    ],
        function (err, results) {
            console.log('end indexing');
           if (err){
               console.log("err:"+err);
               process.exit(-1);
           } else{
               console.log('success');
               process.exit(0);
           }

        });
});