var config = require('../config/config');
var MongoClient = require('mongodb').MongoClient;
MongoClient.connect(config.mongo.dburi, {db:{}, server:{auto_reconnect:true, poolSize:1}}, function (err, db) {
    if (err) {
        console.log('error connecting to mongo');
        throw err;
    }
    //set indexes
    db.collection('profiles', function (err, collection) {
        if (!err) {
            updateAge(err,collection);
            updateGeo(err,collection);
        }
        else {
            console.log('failed to get collection');
            throw err;
        }

    });
});

function updateGeo(err,collection){
    var cursor=collection.find({geo : {$exists : true}});
    cursor.toArray(function(err, docs) {
        docs.forEach(function(doc){
            if (doc.geo && doc.geo instanceof Array && doc.geo.length==2){
                //Convert
                collection.update({_id:doc._id}, {$set:{geo:[doc.geo[0] * 1, doc.geo[1] * 1]}}, {safe:true},onError);
            }
            else{
                collection.update({_id:doc._id}, {$unset:{geo:null}}, {safe:true},onError);
            }
        });
        setIndexes(err,collection);
    });
}

function updateAge(err,collection){
    var cursor=collection.find({age : {$exists : true}});
    cursor.toArray(function(err, docs) {
        docs.forEach(function(doc){
            if (doc.age && typeof doc.age==='string'){
                //Convert
                var age= parseInt(doc.age);
                collection.update({_id:doc._id}, {$set:{age:age}}, {safe:true},onError);
            }
        });
    });
}

function onError(err){
    if (err){
        console.log('error: '+err);
        throw err;
    }
}

function setIndexes(err,collection){
    if (!err) {
        collection.ensureIndex({geo: "2d"}, { bits: 16 }, function (err, result) {
            if (err) {
                console.log('error setting indices: '+err);
                throw err;
            }else{
                console.log("2d set");
            }
        });
        collection.ensureIndex({email: 1}, function (err, result) {
            if (err) {
                console.log('error setting indices: '+err);
                throw err;
            }else{
                console.log("email set");
            }

        });
        collection.ensureIndex({age: 1}, function (err, result) {
            if (err) {
                console.log('error setting indices: '+err);
                throw err;
            }else{
                console.log("age set");
            }
        });

    }
    else {
        console.log('failed to get collection');
        throw err;
    }
}