exports.startServer = function (sharedHttp, sharedBundles) {
    var express = require('express')
        , routes = require('../routes/routes_config')
        , filters = require('../filters/filter')
        , http = sharedHttp || require('http')
        , path = require('path')
        , db = require('../db/Data')
        , config = require('../config/config')
        , csrf = require('../filters/csrf');

    var app = express();

    //Connect to db
    var MongoClient = require('mongodb').MongoClient;
    MongoClient.connect(config.mongo.dburi, {db:{}, server:{auto_reconnect:true, poolSize:10}}, function (err, db) {
        if (err) {
            console.log('error connecting to mongo');
            throw err;
        }


        var MongoStore = require('connect-mongo')(express);
        //Set middleware to set connection
        app.set('db',db);
        app.use(function (req, res, next) {
            req.db = db;
            res.db = db;
            next();
        });
        app.set('cookie-secret', 'Want beer?');
        app.configure('development', function () {
            app.use(express.errorHandler());
        });

        app.configure(function () {
            app.disable('x-powered-by');
            app.set('port', config.web.port);
            app.set('views', path.join(__dirname, '../views'));
            app.set('view engine', 'jade');
            app.use(express.favicon(path.join(__dirname, '../public/images/favicon.ico')));
            app.use(express.logger('dev'));
            app.use(express.bodyParser());
            app.use(express.methodOverride());
            app.use(express.cookieParser(app.get('cookie-secret')));

            app.use(express.compress());//Gzip support

            app.use(express.session({
                key:'sid',
                secret:app.get('cookie-secret'),
                store:new MongoStore({db:db}),
                cookie:{ path:'/', httpOnly:true, maxAge:3600000 * 24 * 14/*2 weeks*/ }
            }));

            //Setup helpers
            var moment = require('moment');
            moment.lang('ru');
            app.locals.moment = moment;
            //Reg needed filters
            filters.register_filters(app);

            app.use(app.router);

            //Register bundles!
            console.log('building bundles');
            var BundleUp = require('bundle-up');

            var needBundling = app.get('env') == "production";
            var staticRoot = needBundling ? '///static.loveplusplus.ru' : "/";
            app.locals.static = function(path) {
                return (staticRoot + path).substr(1);
            }
            BundleUp(app, __dirname + '/assets', {
                staticRoot: path.join(__dirname, '../public'),
                staticUrlRoot:staticRoot,
                bundle:needBundling,
                minifyCss: needBundling,
                minifyJs: needBundling
            });
            console.log('building bundles - ok');
            app.use(express.static(path.join(__dirname, '../public')));

        });

        routes.register_routes(app);

        var server =http.createServer(app);
        //Install socksjs handler
        require('../chat/chat_standalone').createChatServer(db,server);

        server.listen(app.get('port'), function () {
            console.log("Express server listening on port " + app.get('port'));
        });
    });
}