var $ulogin = null;
var views = views || {};
$(function () {
    if (window._csrf && window._csrf !== '') {
        $.ajaxSetup({
            beforeSend:function (xhr) {
                xhr.setRequestHeader('x-csrf-token', window._csrf);
            }
        });
    }

    $("a.exit").click(function (e) {
        var $that = $(this);
        $.post($that.attr('href').substring(1), function () {
            $that.parent().html('выходим...');
            setTimeout(function () {
                window.location.href = '/';
            }, 100);
        });
        e.preventDefault();
        return false;
    });
    $('.what-for-help').click(function (e) {
        $('.what-for').show();
        e.preventDefault();
        return false;
    });
    $('.what-for .close').click(function (e) {
        $('.what-for').hide();
        e.preventDefault();
        return false;
    });

    $('.first-time .close').click(function (e) {
        $('.first-time').remove();
        setFirstTime();
        e.preventDefault();
        return false;
    });

    $('.first-time .select button').click(function (e) {
        var show = $(this).data('open');
        $('.first-time .select').hide();
        $(show).removeClass('hidden');
        setFirstTime();
        e.preventDefault();
        return false;
    });

    $('.content').on('click', '.fancyBox', function (e) {
        if (!e.isPropagationStopped()) {
            $this = $(this);
            var url = $this.css('background-image').replace(/^url\(["']?/, '').replace(/["']?\)$/, '');
            $.fancybox(url,{openEffect:'none',closeEffect:'none',maxWidth:576,type:'image'});
            e.stopPropagation();
            e.preventDefault();
            return false;
        }
    });

    function setFirstTime() {
        //Set cookie
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + 20 * 365);
        document.cookie = 'firstime=1; expires=' + exdate.toUTCString();
    }


    if (!window.canChat) {
        if (!$ulogin && views && views.hasOwnProperty('ulogin')) {
            //Create chat
            $ulogin = $(views['ulogin']());
        }
        $(document).on('click', '.profile-block .send-message', function (e) {
            var $this = $(this);
            $ulogin.triggerButton = $this;
            var $profile = $this.parents('.profile-block').after($ulogin);
            e.preventDefault();
            return false;
        });
    }
});

function onAuthorized(code) {
    if ($ulogin && $ulogin.triggerButton) {
        var $profile = $ulogin.prev();
        $ulogin.detach();
        $.post('/remote_login', {token:code})
            .done(function (data) {
                window.canChat = true;
                //Detach events
                $(document).off('click', '.profile-block .send-message');
                //Load scripts
                loadScript('/chat_init.js?callback=chat.start', function () {
                    //Trigger call
                    $ulogin.triggerButton.trigger('click');
                });
            })
            .fail(function () {
                //TODO: Show something
            });
    } else {
        //Post and reload location
        $.post('/remote_login', {token:code})
            .done(function (data) {
                window.location.reload();
            })
            .fail(function () {
                //TODO: Show something
            });
    }
}

function loadScript(scriptSrc, callback) {
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.onreadystatechange = function () {
        if (this.readyState == 'complete') {
            callback();
        }
    }
    script.onload = callback;
    script.src = scriptSrc;
    head.appendChild(script);
};

