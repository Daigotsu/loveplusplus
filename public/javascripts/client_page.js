$(function () {
    if (History.enabled) {
        var timer;
        function pushUrl(href,el) {
            href= decodeURIComponent(href.replace(/\+/g,  " "));
            $.ajax({
                url: href,
                cache:false,
                dataType:'json'
            })
            .done(function (result) {
                    _gaq.push(['_trackEvent', 'Search', 'Perform']);
                    History.pushState(result, document.title, href);
                })
            .fail(function () {
                    setTimeout(function () {
                        location.href = href
                    }, 0)
            });
        }

        $('#search').submit(function(e){
            var $form = $(this);

            var href='/?'+$form.serialize();
            pushUrl(href);
            e.preventDefault();
            return false;
        });

        $('#search input').change(function(){
            if (timer){
                clearTimeout(timer);
            }
            timer = setTimeout(function(){$('#search').submit();},500);
        });

        $('#profiles').on('click', '.pagination a, a.ajax', function (e) {
            var el = $(this),
                href = el.attr('href');
            pushUrl(href,el);
            e.preventDefault();
            return false;
        });


        History.Adapter.bind(window, 'statechange', function () {
            var state = History.getState(),
                    obj = state.data;
            $('#profiles').html(views['profile_list'](obj));
            $('html, body').scrollTop(0);
        });
    }
});