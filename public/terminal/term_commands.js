$(function () {
        $('html, body').animate({
            scrollTop:$('.content').offset().top
        }, 1000);
        var checkFields = function (obj, fieldsArray) {
            for (var i = 0; i < fieldsArray.length; i++) {
                var field = fieldsArray[i];
                if (typeof field === "string") {
                    if (!obj.hasOwnProperty(field) || obj[field] === undefined || obj[field] === '' || obj[field] === null) {
                        throw new Error('argument missing: ' + field);
                    }
                }
                else if (field.hasOwnProperty('regex') && field.hasOwnProperty('name')) {
                    if (!obj.hasOwnProperty(field.name) || obj[field.name] === undefined) {
                        throw new Error('argument missing: ' + field.name);
                    }
                    if (typeof field.regex === 'function') {
                        if (!field.regex(obj[field.name])) {
                            throw new Error('argument invalid: ' + +field.name);
                        }
                    }
                    else if (!field.regex.test(obj[field.name])) {
                        throw new Error('argument invalid: ' + field.name);
                    }
                }
            }
            return true;
        }

        var validateEmail = function (email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!re.test(email)) {
                throw new Error('Invalid email');
            }
            return email;
        }

        var checkers = {
            "register":[
                {name:'email', regex:validateEmail},
                'name',
                {name:'age', regex:/\d+/},
                'photo',
                'city'
            ],
            "login":[
                {name:'email', regex:validateEmail}
            ],
            "passwd":[
                {name:'email', regex:validateEmail}
            ]
        }

        function sexConverter(sex) {
            if (sex !== 'male' && sex !== 'female') {
                throw new Error('Only male or female allowed in argument "sex"')
            }
            return sex;
        }

        function passwordCheck(password) {
            if (!password || password === '' || password.length == 0) {
                throw new Error('missing or too short password')
            }
            return password;
        }

        function photoCheck(photo) {
            if (!/(\b(https?):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig.test(photo)) {
                throw new Error('bad photo url')
            }
            return photo;
        }


        $('#terminal').terminal(function (command, term) {
            if (command !== '') {
                try {
                    var programm = new commander.Command()
                        .name('lpp> ')
                        .out(function (str) {
                            if (str && str != '') {
                                term.echo(sprintf.apply(null, arguments));
                            }
                            else {
                                term.echo('');
                            }
                        })
                        .error(function (str) {
                            if (str && str != '') {
                                term.error(sprintf.apply(null, arguments));
                            }
                            else {
                                term.error('');
                            }
                        })
                        .version('LPP interpreter 0.1.1');

                    programm
                        .command('register')
                        .description('register new user')
                        .option('-e, --email <email>', 'Your email', validateEmail)
                        .option('-n, --name <name>', 'Your name')
                        .option('-a, --age <age>', 'Your age', parseInt)
                        .option('--city <city>', 'Your location')
                        .option('--photo <photo>', 'Your photo url', photoCheck)
                        .option('--about [about]', 'About you')
                        .option('--salary [salary]', 'Your salary')
                        .option('--sex [sex]', 'Your sex (male|female)', sexConverter)
                        .password()
                        .action(function () {
                        });
                    programm
                        .command('login')
                        .description('login into system')
                        .option('-e, --email <email>', 'E-mail address', validateEmail)
                        .password()
                        .action(function () {
                        });
                    programm
                        .command('rm')
                        .description('remove profile')
                        .option('-f', 'Remove all data')
                        .password()
                        .action(function () {
                        });
                    programm
                        .command('passwd')
                        .description('restore password')
                        .option('-e, --email <email>', 'E-mail address', validateEmail)
                        .option('-k, --key [key]', 'Key that you receive in email\n\t\t\t\t\t call \'passwd\' with -e only to get key')
                        .action(function (cmd) {
                            if (cmd.key && cmd.key !== '') {
                                cmd.password(true, 'Enter new password:');
                            }
                        });
                    programm
                        .command('edit')
                        .description('edit profile')
                        .option('-n, --name [name]', 'Update name')
                        .option('-a, --age [age]', 'Update age', parseInt)
                        .option('--photo [photo]', 'Update photo url', photoCheck)
                        .option('--city [city]', 'Update location')
                        .option('--about [about]', 'Update about')
                        .option('--salary [salary]', 'Update salary')
                        .option('--sex [sex]', 'Update sex (male|female)', sexConverter)
                        .action(function () {
                        });
                    var commandToRun = 'run call ' + command;
                    var regex = /"([^"]+)"|\s*([^"\s]+)/g;
                    var matched = null;
                    var args = [];
                    while (matched = regex.exec(commandToRun)) {
                        args.push(matched[0].replace(/^[\s"]+|[\s"]+$/g, ""));
                    }
                    var result = programm.parse(args);
                    if (result !== undefined && result.args && result.args.length > 0) {
                        for (var cmdId = 0; cmdId < result.args.length; cmdId++) {
                            var command = result.args[cmdId];
                            if (command === 'man' || command === 'help') {
                                var manFor = null;
                                if (cmdId < result.args.length - 1) {
                                    var cmdName = result.args[cmdId + 1];
                                    for (var prgCmd = 0; prgCmd < programm.commands.length; prgCmd++) {
                                        if (programm.commands[prgCmd]._name === cmdName) {
                                            manFor = programm.commands[prgCmd];
                                        }
                                    }
                                }
                                else {
                                    manFor = programm;
                                }
                                if (manFor !== null) {
                                    //Man for main
                                    outputHelpIfNecessary(manFor, ['-h']);
                                } else {
                                    term.error('unknown command');
                                }
                                break;//Break the loop
                            }
                            else if (typeof command === 'object' && command.hasOwnProperty('_name') && (command._name === 'register' || command._name === 'login' || command._name === 'edit' || command._name === 'rm' || command._name === 'passwd')) {
                                if (command._name === 'man') {
                                    throw 'no man';
                                }
                                if (checkers.hasOwnProperty(command._name)) {
                                    checkFields(command, checkers[command._name]);
                                }
                                if (command.passwordNeeded) {
                                    term.set_mask(true);
                                    term.push(function (password) {
                                        term.set_mask(false);
                                        term.pop();
                                        term.disable();
                                        for (var cmdId = 0; cmdId < result.args.length; cmdId++) {
                                            var command = result.args[cmdId];
                                            if (typeof command === 'object' && command.hasOwnProperty('_name')) {
                                                command.password = passwordCheck(password);
                                                processCommand(term, command, function () {
                                                    term.enable();
                                                });
                                            }
                                        }

                                    }, {
                                        prompt:command.promtText || 'Enter password:'
                                    });
                                } else {
                                    //Password not needed
                                    term.disable();
                                    for (var cmdId = 0; cmdId < result.args.length; cmdId++) {
                                        var command = result.args[cmdId];
                                        if (typeof command === 'object' && command.hasOwnProperty('_name')) {
                                            processCommand(term, command, function () {
                                                term.enable();
                                            });
                                        }
                                    }
                                }
                            }
                            else {
                                term.error('unknown command');
                            }
                        }


                    }
                } catch (e) {
                    term.error(new String(e));
                }
            } else {
                term.echo('');
            }
        }, {
            greetings:'Love++ Interpreter\nType \'man\' in the console to see available commands',
            name:'lpp',
            height:320,
            prompt:'lpp> '
        });

        function processCommand(terminal, command, callback) {
            function doQuery() {
                $.ajax({
                    type:"POST",
                    url:'/con',
                    data:data,
                    success:function (res) {
                        if (res.status === 'ok') {
                            //Cookie set and profile ready
                            if (res.message) {
                                terminal.echo('[[b;#00ee11;#000]' + res.message + ']');
                            } else if (res.uid) {
                                terminal.echo('ok. user id=' + res.uid);
                                setTimeout(function () {
                                    location.href = res.redir ? res.redir : '/';
                                }, 3000);
                            }
                        } else {
                            _gaq.push(['_trackEvent', 'Error', 'Terminal', res.message]);
                            terminal.error(res.message);
                        }
                        callback(terminal);
                    },
                    error:function (e) {
                        _gaq.push(['_trackEvent', 'Error', 'Terminal Server', e.statusText]);
                        terminal.error(e.statusText);
                        callback(terminal);
                    },
                    dataType:'json'
                });
            }

            if (command && (command._name === 'register' || command._name === 'login' || command._name === 'edit' || command._name === 'rm' || command._name === 'passwd')) {
                try {
                    var data = {};
                    data.method = command._name;
                    //Build attributes
                    data.obj = attributeBuilders[data.method](command);
                    if (data.obj.city && google && google.maps) {
                        //Try geocode
                        var geocoder = new google.maps.Geocoder();
                        geocoder.geocode({ 'address':data.obj.city, region:'ru'}, function (results, status) {
                                if (status == google.maps.GeocoderStatus.OK && results && results.length > 0) {
                                    if (results[0].address_components) {
                                        var loc = '';
                                        for (var ac = 0; ac < results[0].address_components.length; ac++) {
                                            var types = results[0].address_components[ac].types;
                                            if (types && types.length) {
                                                for (var t = 0; t < types.length; t++) {
                                                    if (types[t] === 'locality' || types[t] === 'country') {
                                                        loc += results[0].address_components[ac].long_name + ", ";
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        loc = loc.replace(/[\s,]+$/, '');
                                        data.obj.city = loc;
                                    }
                                    try {
                                        data.obj.geo = [results[0].geometry.location.lng(), results[0].geometry.location.lat()];
                                    }
                                    catch (err) {
                                    }
                                }
                                doQuery();
                            }
                        );
                    } else {
                        doQuery();
                    }
                }
                catch
                    (err) {
                    terminal.error(err);
                    callback(terminal);
                }
            }
            else {
                terminal.error('unknown command');
                callback(terminal);
            }

        }

        var attributeBuilders = {
            "register":function (command) {
                return stripAttributes(command, ['email', 'password', 'name', 'photo', 'age', 'city', 'skype', 'salary', 'icq', 'jabber', 'vk', 'about', 'sex']);
            },
            "login":function (command) {
                return stripAttributes(command, ['email', 'password']);
            },
            "edit":function (command) {
                return stripAttributes(command, ['name', 'pwd', 'password', 'age', 'city', 'photo', 'skype', 'salary', 'icq', 'jabber', 'vk', 'about', 'sex']);
            },
            "rm":function (command) {
                return stripAttributes(command, ['f', 'password']);
            },
            "passwd":function (command) {
                return stripAttributes(command, ['email', 'key', 'password']);
            }
        }

        function stripAttributes(command, attrArray) {
            var obj = {};
            for (var i = 0; i < attrArray.length; i++) {
                var attr = attrArray[i];
                if (command.hasOwnProperty(attr)) {
                    obj[attr] = command[attr];
                }
            }
            return obj;
        }
    }

)
;
