function initCommonChat() {
    var channel = 'public';
    moment.lang('ru');
    $(document).on('chat-ready', function (evt, data) {
        //Subscribe
        setInterval(function () {
            $('.timestamp').each(function (index, item) {
                var $item = $(item);
                $item.text(moment($item.data('time')).fromNow());
            })
        }, 60000);// 1 min

        var cid = data && data.user && data.user.id;

        $(document).on('chat-reconnected', function (evt, data) {
            chat.send({type:'subscribe', channel:channel});
        });
        chat.send({type:'subscribe', channel:channel});

        var $dialog = $('#commonChat');
        $dialog.find('a.from').popover();
        var msgList = $dialog.find('.msg-list');
        $dialog.find('.msg-holder').scrollTop(msgList.height());
        var $form = $dialog.find('form');
        var $text = $form.find('textarea');

        $text.focus();
        var $sendonenter = $('#sendonenter');
        $text.keypress(function (e) {
            if ($sendonenter.hasClass('active')) {
                if (event.which == 13 && !event.shiftKey) {
                    $form.submit();
                }
            } else {
                if ((e.keyCode === 10 || e.keyCode == 13) && (e.ctrlKey || e.metaKey)) {
                    $form.submit();//!Chrome return key code 10
                }
            }
        });

        $form.submit(function (e) {
            var text = $text.val();
            var msg = {from:cid, to:channel, body:text, received:new Date().getTime()};
            chat.send(msg);
            $(this).find('textarea').val('');
            e.preventDefault();
            return false;
        });

        msgList.on('click', 'a.from', function (e) {
            if (!e.ctrlKey && !e.metaKey) {
                var name = $(this).parents('.msg').data('reply-name');
                if (name && name !== '') {
                    $text.val(name + ', ' + $text.val()).focus();
                }
                e.preventDefault();
                return false;
            }
        });

        $(document).on('chat-message', function (evt, data) {
            if (data.channel === channel) {
                var msg = data.message;
                var item = $(views['dialog_message']({msg:msg, cid:cid}));
                $dialog.find('.msg-list').append(item);
                item.find('a.from').popover();
                $dialog.find('.msg-holder').scrollTop($dialog.find('.msg-list').height());
            }
        });

        $(document).on('chat-join', function (evt, data) {
            if (!evt.isPropagationStopped()) {
                var msg = data;
                var item = $(views['dialog_system']({type:'info', profile:msg.profile, text:'заходит'}));
                $dialog.find('.msg-list').append(item);
                $dialog.find('.msg-holder').scrollTop($dialog.find('.msg-list').height());
                $('#onlineCount').text(msg.online);
                evt.stopPropagation();
                return false;
            }
        });

        /*$(document).on('chat-leave', function (evt, data) {
            if (!evt.isPropagationStopped()) {
                var msg = data;
                var item = $(views['dialog_system']({type:'info', profile:msg.profile, text:'выходит'}));
                $dialog.find('.msg-list').append(item);
                $dialog.find('.msg-holder').scrollTop($dialog.find('.msg-list').height());
                $('#onlineCount').text(msg.online);
                evt.stopPropagation();
                return false;
            }
        });*/

        $(document).on('chat-error', function (evt, data) {
            if (!evt.isPropagationStopped()) {
                var msg = data;
                var item = $(views['dialog_system']({type:'error', profile:null, text:msg.message}));
                $dialog.find('.msg-list').append(item);
                $dialog.find('.msg-holder').scrollTop($dialog.find('.msg-list').height());
                evt.stopPropagation();
                return false;
            }
        });
    });


}