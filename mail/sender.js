var Mandrill = require('mandrill-api').Mandrill;
var config = require('../config/config');
var jade = require('jade');
var fs = require('fs');

exports.sendMail = function (to, subject, templateName, templateData, callback) {
    //Build view
    try {
        fs.readFile(__dirname + '/templates/' + templateName + '.jade', 'utf8', function (err, data) {
            try {
                var fn = jade.compile(data, {filename:__dirname + '/templates/' + templateName + '.jade'});
                templateData.subject = subject;
                templateData.to = to;
                var html = fn(templateData);
                var params={};
                var msg = params.message = {};
                msg.from_email = config.mail.from;
                msg.from_name = config.mail.from_name;
                msg.to = [to];
                msg.subject = subject;
                msg.html = html;
                if (templateData.replyTo){
                    msg.headers = {};
                    msg.headers["Reply-To"]=templateData.replyTo;
                }
                var m = new Mandrill(config.mail.apikey);
                m.messages.send(params, function () {
                    callback(null)
                }, function (err) {
                    callback(err)
                });
            }
            catch (err) {
                callback(err);
            }
        });
    }
    catch
        (err) {
        callback(err);
    }
}