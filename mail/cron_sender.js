var config = require('../config/config');
var crypto = require('crypto');
var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
var sender = require('./sender');

exports.setupNotifications = function () {
    try {
        MongoClient.connect(config.mongo.dburi, {db:{}, server:{auto_reconnect:true, poolSize:1}}, function (err, db) {
            if (err) {
                console.log('error connecting to mongo');
            } else {
                dbReady(db);
            }
        });
    } catch (err) {

    }
}

function dbReady(db) {
    var interval = setInterval(function () {
        onScheduled(db);
    }, 1000 * 60 * 20/*20 min*/)
}

function onScheduled(db) {
    //Get all missed messages for all users
    db.collection('messages', function (err, messageCollection) {
        if (!err) {
            messageCollection.aggregate([
                { $match:{ read:false, last_notify:{ $exists:false}
                }},
                {$group:{
                    _id:'$to',
                    count:{ $sum:1 },
                    messages:{$push:"$body"}
                }}
            ], function (err, unreadData) {
                if (!err) {
                    var ids = unreadData.map(mapIds);
                    db.collection('profiles', function (err, profileCollection) {
                        if (!err) {
                            profileCollection
                                .find({_id:{$in:ids},_removed:null}, {name:true,email:true, photo:true, type:true, profile:true, network:true, city:true, geo:true, hash:true})
                                .sort({last_seen:-1})
                                .toArray(function (err, profiles) {
                                    if (!err) {
                                        //Find needed count
                                        profiles.forEach(function (item) {
                                            for (var i = 0; i < unreadData.length; i++) {
                                                if (unreadData[i]._id === item._id.toString()) {
                                                    item.unread = unreadData[i].count;
                                                    item.messages = unreadData[i].messages;
                                                    break;
                                                }
                                            }
                                        });
                                        profiles.forEach(function (user) {
                                            //Send messages
                                            sender.sendMail({email:user.email, name:user.name}, 'Новые сообщения на Love++', 'new-messages', user, function (err) {
                                                if (!err) {
                                                    //Update to
                                                    messageCollection.update({to:user._id.toString(), read:false, last_notify:{ $exists:false}},
                                                        {$set:{last_notify:Date.now()}},{safe:false,multi:true});
                                                }
                                            });
                                        });
                                    }
                                });
                        }
                    });
                }
            });
        }
    })
}

function mapIds(item) {
    var id = item._id;
    try {
        id = new ObjectID(id);
    } catch (err) {

    }
    return id;
}