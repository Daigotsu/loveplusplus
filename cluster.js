var cluster = require('cluster');
var http = require('http');
var numCPUs = require('os').cpus().length;
var program = require('commander');
var scheduler = require('./mail/cron_sender')

if (cluster.isMaster) {

    program
        .version('0.1.1')
        .option('-t, --threads [threads]', 'specify the thread count', Number, numCPUs)
        .parse(process.argv);
    //Run mail scheduler
    scheduler.setupNotifications();
    // Fork workers.
    for (var i = 0; i < program.threads; i++) {
        console.log('forking cluster ' + i);
        cluster.fork();
    }
    cluster.on('exit', function (worker, code, signal) {
        console.log('worker ' + worker.process.pid + ' died. Restarting');
        cluster.fork();
    });
} else {

    require('./app_start/app.js').startServer(http);//Start with shred server
}