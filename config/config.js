var config = {}

config.mongo = {};
config.web = {};
config.mail = {};

config.web.port = process.env.PORT || 3000;
config.mongo.dburi= process.env.MONGO_URI || process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || "mongodb://127.0.0.1:27017/date-programmer";
config.mail.from = "sender@loveplusplus.ru";
config.mail.from_name = "Love++ mailer";
config.mail.apikey= process.env.MANDRILL_APIKEY || 'sZ4ytwOchAGpdB6WwL5wqw';
config.isProduction = process.env.NODE_ENV === 'production';

config.redis = config.redis || {};
config.redis.url = process.env.REDISCLOUD_URL|| 'redis://rediscloud:rkLXFDHBv9knnWbG@pub-redis-11492.us-east-1-4.3.ec2.garantiadata.com:11492';


module.exports = config;
