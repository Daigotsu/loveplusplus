var ObjectID = require('mongodb').ObjectID;


function getUserProfile(db, userId, callback) {
    db.collection('profiles', function (err, collection) {
        if (!err) {
            //Insert message
            var id = exports.getId(userId);
            collection.findOne({_id:id}, {name:true, photo:true, type:true, profile:true, network:true, city:true, geo:true, hash:true}, function (err, item) {
                if (!err && item) {
                    //Notify
                    callback(null, item);
                } else {
                    callback(err);
                }
            });
        } else {
            callback(err);
        }
    });
}

function getId(id) {
    if (typeof id === 'string') {
        try {
            id = new ObjectID(id);
        } catch (err) {

        }
    }
    return id;
}

exports.getUserProfile = getUserProfile;
exports.getId = getId;