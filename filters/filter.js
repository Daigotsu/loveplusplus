/**
 * Created with JetBrains WebStorm.
 * User: RusanovAl
 * Date: 02.02.13
 * Time: 2:22
 * To change this template use File | Settings | File Templates.
 */
var ObjectID = require('mongodb').ObjectID;
var onlineUsers = require('./online_users');

exports.register_filters = function (app) {
    app.use(filterUser);
    app.use(onError);
}

function filterUser(req, res, next) {
    //Load user
    res.locals.path = req.path;
    res.locals.url = req.url;
    res.locals.full_path = require('url').resolve('http://' + req.headers.host, req.path);
    res.locals.first_time = !req.cookies.firstime;

    res.locals.chatAvailible = req.session && (req.session.profile || req.session.user);

    if (req.session.profile) {
        //Set auth profile
        res.locals.profile = req.session.profile;
    }
    if (req.session.user) {
        try {
            req.db.collection('profiles', function (err, collection) {
                //Check for user
                try {
                    collection.findOne({_id:new ObjectID(req.session.user)}, function (err, item) {
                        if (item != null) {
                            res.locals.currentUser = {_id:item._id, name:item.name, sex:item.sex, updated:item.updated};
                            res.locals.authenticated = req.session.user !== undefined;
                            res.locals.currentUserKeys = {_removeKey:item._removeKey};//TODO: add needed keys here
                            //Add last_seen timestamp
                            collection.update({_id:item._id},{$set:{last_seen:Date.now()}},{safe:false});
                        }
                        next();
                    })
                }
                catch (err) {
                    next(err);
                }
            });
        } catch (err) {
            next(err);
        }
    } else {
        next();
    }
}

function onError(err, req, res, next) {
    if (err) {
        if (req.xhr) {
            res.json({status:'fail', message:err.toString()}, err.status);
        }
        else {
            res.render('error', {message:err.toString()});
        }
    } else {
        next();
    }
}

