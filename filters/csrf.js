var ObjectID = require('mongodb').ObjectID;

module.exports = function csrf(db, options) {
    options = options || {};
    var value = options.value || defaultValue;


    return function (req, res, next) {
        // generate CSRF token
        db.collection('csrf-tokens', function (err, collection) {
            if (err) {
                return next();
            }
            //insert token
            //TODO: Maybe should add session id
            collection.insert({issued:new Date(), ip:req.ip}, {safe:true}, function (err, items) {
                if (err) {
                    next();
                }
                res.locals._csrf = items[0]._id;
                // ignore these methods
                if ('GET' == req.method || 'HEAD' == req.method || 'OPTIONS' == req.method) return next();

                var val = value(req);
                if (!val) {
                    next(403);
                }
                else {
                    try {
                        collection.findOne({_id:new ObjectID(val), ip:req.ip, issued:{$lte:new Date()}}, function (err, item) {
                            // determine value
                            if (err) {
                                next(403);
                            } else {
                                next();
                            }
                        });
                    } catch (err) {
                        next();
                    }
                }
            });
        });
    };

    function defaultValue(req) {
        return (req.body && req.body._csrf)
            || (req.query && req.query._csrf)
            || (req.headers['x-csrf-token']);
    }
};
