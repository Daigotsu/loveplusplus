/*
 * GET home page.
 */
var ObjectID = require('mongodb').ObjectID;
var assert = require('assert');
var validation = require('../validators/validation');
var sender = require('../mail/sender');
var crypto = require('crypto');
var config = require('../config/config');

exports.more = function (req, res) {
    res.render('more', { title:'Express' });
};

exports.index = exports.catalog = function (req, res) {
    req.db.collection('profiles', function (err, collection) {
        //Check for user
        var page = parseInt(req.param("page")), pageSize = 10;
        if (!page) {
            page = 1;
        }
        res.locals.query = req.query;
        try {
            var filter = config.isProduction ? {_validated:true} : {};
            filter._removed = null;
            if (req.query) {

                if (req.query.lng && req.query.lat) {
                    var lng = parseFloat(req.query.lng);
                    var lat = parseFloat(req.query.lat);
                    filter.geo = {
                        $within:{$centerSphere:[
                            [ lng, lat ] ,
                            80 / 6378.137
                        ]},
                        $exists:true};//80km
                }
                if (req.query.agegt) {
                    filter.age = filter.age || {};
                    filter.age["$gte"] = parseInt(req.query.agegt);
                }
                if (req.query.agelt) {
                    filter.age = filter.age || {};
                    filter.age["$lte"] = parseInt(req.query.agelt);
                }
                if (req.query.sex) {
                    filter.sex = req.query.sex;
                }
                if (!req.query.lat && !req.query.lng && req.query.query) {
                    var cityQuery = req.query.query.replace(/[\w,]+/g, ' ').split(' ');
                    if (cityQuery.length > 0) {
                        var regexp = [];
                        for (var i = 0; i < cityQuery.length; i++) {
                            if (cityQuery[i] !== '') {
                                regexp.push(cityQuery[i]);
                                if (i < cityQuery.length - 1) {
                                    regexp.push('\\W+')
                                }
                            }
                        }
                        if (regexp.length > 0) {
                            filter.city = {
                                $regex:regexp.join(''),
                                $options:'i'
                            }
                        }
                    }
                }
            }

            collection.count(filter, function (err, count) {
                var cursor = collection.find(filter).sort({updated:-1}).limit(pageSize).skip((page - 1) * pageSize);
                cursor.toArray(function (err, docs) {
                    if (err) {
                        res.render('error', { title:'Express', error:err});
                    } else {
                        var filteredDocs = [];
                        filteredDocs = docs.map(function (item) {
                            return {_id:item._id.toString(), name:item.name, photo:item.photo, age:item.age, city:item.city, about:item.about, salary:item.salary, last_seen:item.last_seen, hash:item.hash};
                        });
                        var data = { profiles:filteredDocs, totalPages:Math.floor((count - 1) / pageSize) + 1, page:page };
                        if (req.xhr) {
                            var merged = mergeLocals(data || {}, res.locals, ['currentUser', 'url']);
                            res.json(merged);
                        } else {
                            res.render('catalog', data);
                        }
                    }
                });
            });
        } catch (err) {
            res.render('error', { title:'Express', error:err});
        }
    });

};

function mergeLocals(data, locals, fields) {
    var key;
    if (data && locals) {
        data.locals = {};
        for (var i = 0; i < fields.length; i++) {
            data.locals[fields[i]] = locals[fields[i]];
        }
    }
    return data;
}

exports.register = function (req, res) {
    res.render('register', { title:'Express' });
};

exports.profile = function (req, res) {
    var userId = req.param('userid');
    if (userId && typeof userId === 'string') {
        require('../db/data_helpers').getUserProfile(req.db, userId, function (err, item) {
            if (item != null) {
                res.render('profile', { user:item});
            } else {
                res.render('profile_not_found', { uid:userId});
            }
        });
    } else {
        res.render('profile_not_found', {  uid:userId});
    }
};

exports.exit = function (req, res) {
    var destroyed = false;
    if (req.session) {
        req.session.destroy();
        destroyed = true;
    }

    if (req.xhr) {
        res.json({destroyed:destroyed});
    }
    else {
        res.redirect('/');
    }
}


exports.message = function (req, res) {
    if (req.body.message && req.session.profile && req.body.message.trim() !== '') {
        //Sending message to
        var userId = req.param('userid');
        if (userId && typeof userId === 'string') {
            req.db.collection('profiles', function (err, collection) {
                //Check for user
                try {
                    collection.findOne({_id:new ObjectID(userId), _removed:null}, function (err, item) {
                        if (item != null) {
                            sender.sendMail({email:item.email, name:item.name}, 'Новое сообщение на Love++',
                                'new-message',
                                {user:item, from:req.session.profile, replyTo:req.session.profile.email, text:req.body.message}, function () {

                                });
                            res.locals.sent = true;
                            res.render('message', { title:item.name, user:item, sent:true});
                        } else {
                            res.render('profile_not_found', { title:'Профиль не найден', uid:req.param('userid')});
                        }
                    })
                } catch (err) {
                    res.render('profile_not_found', { title:'Профиль не найден', uid:req.param('userid')});
                }
            });
        }
    }
    else if (req.body.token) {
        //Got auth token. Get the profile
        var request = require('request');
        request('http://ulogin.ru/token.php?token=' + req.body.token + '&host=' + req.headers.host, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                try {
                    var profile = JSON.parse(body);
                    req.session.profile = profile;
                    res.locals.profile = profile;
                    //Save to collection
                    req.db.collection('profiles', function (err, collection) {
                        if (!err) {
                            profile._id = 'guest' + crypto.createHash('md5').update(profile.email.toLowerCase().trim()).digest("hex");
                            profile.type = 'guest';
                            profile.name = profile.first_name + ' ' + profile.last_name;
                            profile.photo = profile.photo_big || profile.photo;
                            collection.save(profile, {upsert:true, safe:false});
                        }
                    });
                } catch (err) {

                }
            }
            return res.redirect('/send/' + req.param('userid'));
        })
    } else {
        sendMessages(req, res);
    }
}

function sendMessages(req, res) {
    var userId = req.param('userid');
    if (userId && typeof userId === 'string') {
        req.db.collection('profiles', function (err, collection) {
            //Check for user
            try {
                collection.findOne({_id:new ObjectID(userId)}, function (err, item) {
                    if (item != null) {
                        delete item.password;
                        delete item.email;
                        res.render('message', { title:item.name, user:item});
                    } else {
                        res.render('profile_not_found', { title:'Профиль не найден', uid:req.param('userid')});
                    }
                })
            } catch (err) {
                res.render('profile_not_found', { title:'Профиль не найден', uid:req.param('userid')});
            }
        });
    }
}

function stripAttributes(command, attrArray) {
    var obj = {};
    for (var i = 0; i < attrArray.length; i++) {
        var attr = attrArray[i];
        if (command.hasOwnProperty(attr)) {
            obj[attr] = command[attr];
        }
    }
    return obj;
}