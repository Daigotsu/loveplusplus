var async = require('async');
var crypto = require('crypto');
var ObjectID = require('mongodb').ObjectID;
var _ = require('underscore');

exports.chat = function (req, res) {
    //TODO: think about storing last token in session, maybe it's better idea
    //Get the profile info
    req.db.collection('chat-tokens', function (err, collection) {
        if (err) {
            return res.send(400, 'Token failed');
        }
        var profile = {};
        if (req.session.user) {
            profile.id = req.session.user;
            profile.type = 'builtin';
        } else if (req.session.profile && req.session.profile.email) {
            profile.id = 'guest' + crypto.createHash('md5').update(req.session.profile.email.toLowerCase().trim()).digest("hex");
            profile.type = 'guest';
        }
        if (!profile.id) {
            return res.send(403, 'Not authorized');
        }
        //Do parallel async
        async.parallel({
                token:function (callback) {
                    //Generate token
                    collection.insert({issued:new Date(), ip:req.ip, user:profile}, {safe:true}, function (err, items) {
                        var token = null;
                        if (!err && items && items.length) {
                            token = items[0]._id.toString();
                        }
                        callback(err, token);
                    });
                },
                messages:function (callback) {
                    req.db.collection('messages', function (err, collection) {
                        if (!err) {
                            collection.count({to:profile.id, read:false}, callback);
                        } else {
                            callback(err, null);
                        }
                    });
                }},
            // optional callback
            function (err, results) {
                if (!err && results && results && results.token) {
                    res.jsonp({token:results.token, user:profile, unread:results.messages});//Sending script
                } else {
                    return res.send(400, 'Session failed');
                }
            });
    });
}


exports.chatData = function (req, res) {
    //Get the profile info
    var userId = '';
    if (req.session.user) {
        userId = req.session.user;
    } else if (req.session.profile && req.session.profile.email) {
        userId = 'guest' + crypto.createHash('md5').update(req.session.profile.email.toLowerCase().trim()).digest("hex");
    }
    if (!userId || userId === '') {
        return res.render('unauthorized');
    }
    queryUserData(req.db, userId, function (err, results) {
        if (err) {
            res.render('error', { error:err});
        } else {
            res.render('chat', results);
        }
    });
}

function queryUserData(db, userId, callback) {
    db.collection('messages', function (err, collection) {
        async.parallel(
            {
                from:function (callback) {
                    collection.distinct('from', {to:userId}, callback);
                },
                to:function (callback) {
                    collection.distinct('to', {from:userId}, callback);
                }
            },
            function (err, data) {
                if (!err && data.from && data.to) {
                    var ids = _.uniq(data.from.concat(data.to));
                    var mappedIds = ids.map(mapIds);
                    async.parallel(
                        {
                            profiles:function (callback) {
                                db.collection('profiles', function (err, profileCollection) {
                                    if (err) {
                                        callback(err);
                                    } else {
                                        profileCollection
                                            .find({_id:{$in:mappedIds}}, {name:true, photo:true, type:true, profile:true, network:true, city:true, geo:true, hash:true})
                                            .sort({last_seen:-1})
                                            .toArray(callback);
                                    }
                                })
                            },
                            unread:function (callback) {
                                db.collection('messages', function (err, messageCollection) {
                                    if (err) {
                                        callback(err);
                                    } else {
                                        messageCollection.aggregate([
                                            { $match:{ to:userId, read:false }},
                                            {$group:{
                                                _id:'$from',
                                                count:{ $sum:1 }
                                            }}
                                        ], callback);
                                    }
                                })
                            }
                        }, function (err, data) {
                            callback(err, data);
                        });
                } else {
                    callback(err);
                }
            }
        );
    });
}

function mapIds(item) {
    var id = item;
    try {
        id = new ObjectID(id);
    } catch (err) {

    }
    return id;
}

exports.chatHistory = function (req, res) {
    //Get the profile info
    var userId = '';
    if (req.session.user) {
        userId = req.session.user;
    } else if (req.session.profile && req.session.profile.email) {
        userId = 'guest' + crypto.createHash('md5').update(req.session.profile.email.toLowerCase().trim()).digest("hex");
    }
    if (!userId || userId === '') {
        res.json(403);
    }
    //Get the parameter
    var toUserId = req.param('to');
    if (!toUserId || typeof toUserId !== 'string') {
        res.json(403);
    }
    req.db.collection('messages', function (err, collection) {
        if (!err) {
            collection.find({$or:[
                {to:toUserId, from:userId},
                {from:toUserId, to:userId}
            ]}).sort({received:-1}).limit(100).toArray(function (err, items) {
                if (!err) {
                    //Mark as readed
                    collection.update({to:userId, from:toUserId}, {$set:{read:true}}, {safe:false, multi:true});
                    res.json({messages:items.reverse(), cid:userId});
                } else {
                    res.json(400);
                }
            });
        } else {
            res.json(400);
        }
    });
}

exports.chatRoom = function (req, res) {
    //Get the profile info
    var userId = '';
    if (req.session.user) {
        userId = req.session.user;
    } else if (req.session.profile && req.session.profile.email) {
        userId = 'guest' + crypto.createHash('md5').update(req.session.profile.email.toLowerCase().trim()).digest("hex");
    }
    if (!userId || userId === '') {
        return res.render('unauthorized');
    }
    req.db.collection('messages', function (err, collection) {
        if (!err) {
            collection.find({to:'public'}).sort({received:-1}).limit(100).toArray(function (err, items) {
                if (!err) {
                    res.render('common_chat', {messages:items.reverse(), cid:userId});
                } else {
                    res.render('error');
                }
            });
        } else {
            res.render('error');
        }
    });

}