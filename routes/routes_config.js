//All routes registered here
var index = require('./index'),
    console = require('./console'),
    verify = require('./verify'),
    templates = require('./templates'),
    chat = require('./chat'),
    csrf = require('../filters/csrf'),
    onlineUsers = require('../filters/online_users'),
    remoteLogin = require('./remote_login');


exports.register_routes = function(app){
    //NOTE: Add csrf for routes that needs protection!
    var csrfFilter = csrf(app.get('db'));
    var online = onlineUsers(app.get('db'),app.get('pubsub'),{ttl:120});
    //Compiled templates
    var clientTemplates = templates.render(app)
    app.get('/templates.js', clientTemplates.catalog);
    app.get('/chat-templates.js', clientTemplates.chat);

    app.get('/', /*TODO: Disable for now online,*/ index.index);//TODO: Set online where appropriate
    app.get('/register',csrfFilter, index.register);
    app.get('/profile/:userid', index.profile);
    //app.get('/send/:userid',csrfFilter, index.message);
    //app.post('/send/:userid',csrfFilter, index.message);

    //POSTS
    app.post('/con',csrfFilter, console.console);
    app.post('/exit', index.exit);
    app.post('/remote_login', remoteLogin.login);

    //Validations
    app.get('/verify/:validationKey', verify.validate);
    app.get('/remove/:validationKey',csrfFilter, verify.remove);
    app.post('/remove/:validationKey',csrfFilter, verify.remove);

    //Chat
    app.get('/chat',chat.chatData);
    app.get('/chat/history/:to',chat.chatHistory);
    app.get('/chat_init.js', chat.chat);
    app.get('/chat-history/:token', chat.chatData);
    app.get('/chat-room', chat.chatRoom);//Common chat room
};