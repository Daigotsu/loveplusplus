var ObjectID = require('mongodb').ObjectID;
var assert = require('assert');
var validation = require('../validators/validation');
var sender = require('../mail/sender');
var crypto = require('crypto');
var config = require('../config/config');

exports.validate = function (req, res) {
    var validationKey = req.param('validationKey');
    if (validationKey && typeof validationKey === 'string') {
        req.db.collection('profiles', function (err, collection) {
            //Check for user
            try {
                collection.update({_validationKey:validationKey}, {$set:{_validated:true}}, {safe:true},
                    function (err) {
                        if (err) res.status(404).render('validate_failed', {message:'Ошибка валидации'});
                        else res.render('validated_successfully', {title:'Validated!'});
                    });

            } catch (err) {
                res.status(404).render('validate_failed', {message:'Ошибка валидации'});
            }
        });
    } else {
        res.status(404).render('validate_failed', {message:'Неверный ключ'});
    }
};

exports.remove = function (req, res) {
    var validationKey = req.param('validationKey');
    if (validationKey && typeof validationKey === 'string') {
        if (res.locals.currentUserKeys && validationKey !== res.locals.currentUserKeys._removeKey) {
            return res.status(500).render('validate_failed', {message:'Ошибка удаления. Ошибка авторизации'});
        }
        if ('POST' == req.method && req.body && req.body.delete === 'УДАЛИТЬ') {
            req.db.collection('profiles', function (err, collection) {
                //Check for user
                try {
                    collection.update({_removeKey:validationKey}, {$set:{_removed:true}, $unset:{_removeKey:''}}, {safe:true},
                        function (err) {
                            if (err) res.status(500).render('validate_failed', {message:'Ошибка удаления'});
                            else {
                                if (req.session) {
                                    req.session.destroy();
                                }
                                res.render('removed_successfully', {title:'Validated!'});
                            }
                        });

                } catch (err) {
                    res.status(500).render('validate_failed', {message:'Ошибка удаления'});
                }
            });
        } else {
            res.render('remove_profile');
        }
    } else {
        res.status(404).render('validate_failed', {message:'Неверный ключ'});
    }
};



