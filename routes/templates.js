var fs = require('fs');
var jade = require('jade');

exports.render = function (app) {
    this.app = app;
    var viewOptions = { compileDebug:false, self:false };

    function loadTemplate(viewpath) {
        var fpath = app.get('views') + viewpath,
            str = fs.readFileSync(fpath, 'utf8');

        viewOptions.filename = fpath;
        viewOptions.client = true;

        return jade.compile(str, viewOptions).toString();
    }

    return {
        catalog:function (req, res) {
            var str = 'var views = { '
                + '"profile_list": (function(){ return ' + loadTemplate('/includes/profile_list.jade') + ' }()),'
                + '"ulogin": (function(){ return ' + loadTemplate('/includes/ulogin.jade') + ' }()),'
                + '"inline_message": (function(){ return ' + loadTemplate('/chat/inline_message.jade') + ' }()),'
                + '"dialog": (function(){ return ' + loadTemplate('/chat/dialog.jade') + ' }()),'
                + '"dialog_contact": (function(){ return ' + loadTemplate('/chat/dialog_contact.jade') + ' }()),'
                + '"message_notification": (function(){ return ' + loadTemplate('/chat/message_notification.jade') + ' }()),'
                + '"dialog_system": (function(){ return ' + loadTemplate('/chat/dialog_system.jade') + ' }()),'
                + '"dialog_message": (function(){ return ' + loadTemplate('/chat/dialog_message.jade') + ' }())'
                + '};'

            res.set({ 'Content-type':'text/javascript' }).send(str);
        },
        chat:function (req, res) {
            var str = 'var chat = chat || {}; chat.template = (function(){ return ' + loadTemplate('/chat/chat.jade') + ' }());';
            res.set({ 'Content-type':'text/javascript' }).send(str);
        }
    }
}