var sockjs = require('sockjs');
var ObjectID = require('mongodb').ObjectID;
var EventEmitter = require('events').EventEmitter;
var async = require('async');

exports.createChatServer = function (mongoConnection, hub, httpServer) {
    var profileCache = {};
    var urlParse = /^\/chat_poll\/([0-9-.a-z_]*).+/i;
    var messageAddressParse = /^msg([0-9-.a-z_]+)\/([0-9-.a-z_]+)$/i;
    var db = mongoConnection;


    var sockjs_opts = {
        sockjs_url:"http://cdn.sockjs.org/sockjs-0.3.min.js",
        websocket:false, //We don't use socket for now because heroku doesn't support it for now
        log:function (severity, message) {
            console.log(severity + ":" + message);
        }
    };
    var server = sockjs.createServer(sockjs_opts);

    function ChatEmitter(name) {
        this.name = name;
        EventEmitter.call(this);
        ChatEmitter.prototype.__proto__ = EventEmitter.prototype;
    }

    var emitter = new ChatEmitter('chat-emitter');
    var message_emitter = new ChatEmitter('chat-message-emitter');
    emitter.setMaxListeners(0);
    message_emitter.setMaxListeners(0);

    function onPatternMessage(pattern, channel, message) {
        //Posted when user retrieve message through redis
        //Got message from subscribe
        console.log('new pattern message on ' + channel + ': ' + message);
        if (channel.indexOf('msg') == 0) {
            //It's message. Parse, save and distribute
            var address = messageAddressParse.exec(channel);
            if (address.length == 3) {
                var msg = JSON.parse(message);
                msg.from = address[1];
                msg.to = address[2];
                msg.received = Date.now();
                msg.read = false;
                //Save to mongo
                //Here we should load profile and save message in parallel
                async.parallel({
                    message:function (callback) {
                        db.collection('messages', function (err, collection) {
                            if (!err) {
                                //Insert message
                                collection.insert(msg, function (err, items) {
                                    if (!err && items && items.length) {
                                        //Notify
                                        callback(null, items[0]);
                                    } else {
                                        callback(err);
                                    }
                                });
                            } else {
                                callback(err);
                            }
                        });
                    },
                    profile:function (callback) {
                        if (profileCache.hasOwnProperty(msg.from)) {
                            callback(null, profileCache[msg.from]);
                        } else {
                            //Search in DB
                            db.collection('profiles', function (err, collection) {
                                if (!err) {
                                    //Insert message
                                    var id = msg.from;
                                    try {
                                        id = new ObjectID(id);
                                    } catch (err) {

                                    }
                                    collection.findOne({_id:id}, {name:true, photo:true, type:true, profile:true, network:true, city:true, geo:true, hash:true}, function (err, item) {
                                        if (!err && item) {
                                            //Notify
                                            profileCache[msg.from] = item;
                                            callback(null, item);
                                        } else {
                                            callback(err);
                                        }
                                    });
                                } else {
                                    callback(err);
                                }
                            });
                        }
                    }
                }, function (err, results) {
                    if (!err && results.message && results.profile) {
                        var message = results.message;
                        message.fromProfile = results.profile;
                        message_emitter.emit(msg.to, message);
                    }
                });

            }
        }
    }

    function onPubMessage(channel, message) {
        //Global messages
        emitter.emit(channel, JSON.parse(message));
    }

    function subsribeToChannels() {
        //TODO:Remove offline online for now
        //hub.sub.subscribe("online");
        //hub.sub.subscribe("offline");
        hub.sub.psubscribe("msg*");//Subscribe to messages between users. Messages are published in format 'msg<userid_from>/<userid_to>'
    }

    hub.sub.on('reconnected', subsribeToChannels);
    subsribeToChannels();

    //Subscribe to events
    //TODO: Maybe this events needs to be resubscribed to in case of disconnect
    hub.sub.on('pmessage', onPatternMessage);

    //TODO:Remove offline online for now
    //hub.sub.on('message', onPubMessage);

    //Setup listeners
    server.on('connection', function (conn) {
        if (!conn) {
            console.log(" [.] open event received. con is null. closing");
            return;
        }
        //Add send method for objects
        //TODO: Move this to Connection prototype
        conn.send = function (obj) {
            conn.write(JSON.stringify(obj))
        }
        conn.fatalError = function (message) {
            conn.send({type:'error', message:message});
            conn.close(400, message);
        }
        conn.on('data', function (message) {
            onData(conn, message);
        });

        //Bug fix for heroku not closing connections
        onOpen(conn);
        console.log(" [.] open event received");
        var t = setInterval(function () {
            try {
                conn._session.recv.didClose();
            } catch (x) {
            }
        }, 15000);
        conn.on('close', function () {
            console.log(" [.] close event received");
            clearInterval(t);
            onClose(conn);
        });
    });

    function onOpen(connection) {
        var parsed = urlParse.exec(connection.url);
        if (parsed.length > 1) {
            connection.token = parsed[1];
        } else {
            connection.fatalError('bad token');
        }
        //Query mongo. Check if provided token is correct
        db.collection('chat-tokens', function (err, collection) {
            if (err) {
                connection.fatalError('error opening db');
            } else {
                try {
                    collection.findOne({_id:new ObjectID(connection.token)}, function (err, item) {
                        // determine value
                        if (err) {
                            connection.fatalError('token not found');
                        } else {
                            //Authorized
                            if (item.user && item.user.id) {
                                connection.authorized = true;
                                connection.userid = item.user.id;
                                connection.user = item.user;
                                onAuthorized(connection);
                                hub.pub.publish("online", JSON.stringify(connection.user));
                            } else {
                                connection.fatalError('token not belongs to any user');
                            }
                        }
                    });
                }
                catch (err) {
                    connection.fatalError('bad token');
                }
            }
        });
    }


    function updateUserOnline(connection) {
        if (connection.userid && connection.user && connection.user.type === 'builtin') {
            //Update last_visit
            db.collection('profiles', function (err, collection) {
                if (!err) {
                    try {
                        collection.update({_id:new ObjectID(connection.userid)}, {$set:{last_seen:Date.now()}}, {safe:false});
                    } catch (err) {

                    }
                }
            });
        }
    }

    function onData(connection, message) {
        try {
            var messageObject = JSON.parse(message);
            if (messageObject.type) {
                //Handle command
                if (commandHandler.hasOwnProperty(messageObject.type)) {
                    commandHandler[messageObject.type](connection, message);
                }
            } else {
                //It's message
                onMessage(connection, messageObject);
                updateUserOnline(connection);
            }
        } catch (err) {
            connection.close();
        }
    }

    function onMessage(connection, message) {
        //Publish to hub
        //Make an message channel id. It's parsed in hub.sub.on('message') above
        if (message && message.to && message.body && message.from === connection.userid && connection.user) {
            var msgChannel = 'msg' + connection.userid + '/' + message.to;
            hub.pub.publish(msgChannel, JSON.stringify(message));
        }
    }

    //Callbacks
    function getOnlineCallback(conn) {
        return function (user) {
            conn.send({type:'online', user:user});
        };
    }

    function getOfflineCallback(conn) {
        return function (user) {
            conn.send({type:'offline', user:user});
        };
    }

    function getMessageCallback(conn) {
        return function (message) {
            conn.send({type:'message', message:message});
        };
    }

    function onAuthorized(connection) {
        //Setup callbacks
        connection.onlineCallback = getOnlineCallback(connection);
        connection.offlineCallback = getOfflineCallback(connection);
        connection.messageCallback = getMessageCallback(connection);

        emitter.on('online', connection.onlineCallback);
        emitter.on('offline', connection.offlineCallback);
        message_emitter.on(connection.userid, connection.messageCallback);
    }

    function onClose(connection) {
        if (connection.authorized && connection.user) {
            hub.pub.publish("offline", JSON.stringify(connection.user));

            emitter.removeListener('online', connection.onlineCallback);
            emitter.removeListener('offline', connection.offlineCallback);
            message_emitter.removeListener(connection.userid, connection.messageCallback);
        }
    }

    var commandHandler = {};
    commandHandler.read = function (connection, message) {
        //TODO: Здесь мы будем обрабатывать пометку сообщений как прочитаных. Клиент будет посылать {type:'read',ids:['ffff','fffff','fff333']}
        //TODO: Клиент посылает раз секунд так в 10 чтоб не засирать сервак
    }

    server.installHandlers(httpServer, {prefix:'/chat_poll/[0-9-.a-z_]*'});
}